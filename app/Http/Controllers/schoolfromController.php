<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class schoolfromController extends Controller
{
    public function index()
    {


        return view('students.schoolfrom');
    }

    public function store(Request $request)
    {

        DB::table('school')->insert([
            'name' => $request->name,
            'address' => $request->address,

        ]);

        return redirect('/student');
    }
}
